char table[17];

room()
{
    return strlen(table)!=16;
}

int D;
m(int x, int y)
{
    return !D? x+y*4 
             : D==1? 3-x+y*4 
                   : D==2? y+x*4
                         : y + (3-x)*4;
}

move()
{
    int x,y,X;
    for(y=0;y<4;++y)
    {
        for(X=0,x=1;x<4;++x)
        {
            if(table[m(x,y)])
            {
                if(table[m(x,y)]==table[m(X,y)])
                {
                    table[m(x,y)]=0;
                    table[m(X,y)]++;
                }
                X=x;
            }
        }
    }
}

merge()
{
    int x,y;
    for(y=0;y<4;++y)
        for(x=0;x<3;++x)
            if (!table[m(x,y)]&&table[m(x+1,y)])
            {
                table[m(x,y)]=table[m(x+1,y)];
                table[m(x+1,y)]=0;
                return 1;
            }
    return 0;
}

comb()
{
    int x,y;
    for(y=0;y<3;++y)
        for (x=0;x<3;++x)
        {
            char p = table[x+y*4];
            if (table[x+1+y*4]==p || table[x+4+y*4]==p)
                return 1;
        }
    return 0;
}

randompos()
{
    if (!room())return 0;
    int p;
    while(table[p=(rand()&15)]);
    return table[p]=1;
}
bar()
{
    int i=0;
    putchar(10);
    while(i<21)
        putchar(i++%5?'-':'+');
    putchar(10);
}

print()
{
    int t,i=0;
    while(i<16)
    {
        if (!(i&3)) {bar();putchar('|');}
        if(t=table[i++])
            printf("%4u|",1<<t);
        else
            printf("    |");
       
    }
    bar();putchar(10);
}
int w;
fini()
{
    int i=16;
    for (i=0;i<17;++i)
        if (table[i]==11) 
        {
            w=1;
            return 0;
        }
    return 1;
}

#include<termios.h>
char gc() {
        char buf = 0;
        struct termios old = {0};
        if (tcgetattr(0, &old) < 0)
                perror("tcsetattr()");
        old.c_lflag &= ~ICANON;
        old.c_lflag &= ~ECHO;
        old.c_cc[VMIN] = 1;
        old.c_cc[VTIME] = 0;
        if (tcsetattr(0, TCSANOW, &old) < 0)
                perror("tcsetattr ICANON");
        if (read(0, &buf, 1) < 0)
                perror ("read()");
        old.c_lflag |= ICANON;
        old.c_lflag |= ECHO;
        if (tcsetattr(0, TCSADRAIN, &old) < 0)
                perror ("tcsetattr ~ICANON");
        return (buf);
}


main()
{
    srand(time(0));
    table[16]=0;
    int r=1;
    while( fini() && (randompos() || comb()) )
    {
        print();
        switch(gc()){
            case 'a':
                D=0;
                break;
            case 'd':
                D=1;
                break;
            case 'w':
                D=2;
                break;
            case 's':
                D=3;
                break;
        }
        move();
        while(merge());
    }
    puts(w?"you win\n":"you lose\n");
}
