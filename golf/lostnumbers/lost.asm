global start
extern _write
extern _exit

section .data
buf  db  0    

section .text

myWrite:
    mov     [buf],al
    push    1
    push    buf
    push   1
    call    _write
    ret

start:
    mov     al,0x10
    call    myWrite
    push    0
    call    _exit

