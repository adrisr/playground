#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <portaudio.h>

#define SAMPLE_RATE 22050

#define PA_CHECK(ACT) do { \
        err = (ACT); \
        if (err != paNoError) \
            goto pa_error; \
    } while(0)

volatile float level = 0.5;

int callback(const void *input,
             void *output,
             unsigned long frameCount,
             const PaStreamCallbackTimeInfo* timeInfo,
             PaStreamCallbackFlags statusFlags,
             void *userData) {
    float *in = (float*) input;
    float *out = (float*) output;
    float saved_level = level;
    for (int i=0; i<frameCount; ++i) {
        out[i] = - saved_level * in[i];
    }
    return 0;
}

int menu() {
    fprintf(stderr, "---- Processing. Press Q to finish ----\n");
    fprintf(stderr, "Level: %f\n", level);
    switch (getchar()) {
        case EOF:
        case 'Q':
        case 'q':
            return 0;
        
        case 'a':
            level *= 1.1;
            break;

        case 'z':
            level /= 1.1;
            break;

        case 'f':
            level = -level;
            break;
    }
    return 1;
}


int main(int argc, char *argv[]) {
    PaError err;
    PA_CHECK( Pa_Initialize() );
    
    PaStream *stream;
    PA_CHECK( Pa_OpenDefaultStream(&stream,
                1,
                1,
                paFloat32,
                SAMPLE_RATE,
                256,
                callback,
                NULL) );
    
    PA_CHECK( Pa_StartStream( stream ) );

    while (menu());

    PA_CHECK( Pa_StopStream (stream) );
    PA_CHECK( Pa_CloseStream (stream) );
    PA_CHECK( Pa_Terminate() );
    return 0;

pa_error:
    fprintf(stderr, "Error initializing PortAudio: %s\n",
            Pa_GetErrorText(err));
    return 1;
}

