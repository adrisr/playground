package numberdesc

// thanks http://en.wikipedia.org/wiki/English_numerals

var units = []string{
	"zero",
	"one",
	"two",
	"three",
	"four",
	"five",
	"six",
	"seven",
	"eight",
	"nine",
	"ten",
	"eleven",
	"twelve",
	"thirteen",
	"fourteen",
	"fifteen",
	"sixteen",
	"seventeen",
	"eighteen",
	"nineteen",
}

var tens = []string{
	"twenty",
	"thirty",
	"forty",
	"fifty",
	"sixty",
	"seventy",
	"eighty",
	"ninety",
}

type power struct {
	scale int
	name  string
}

var powers = []power{
	{1000000000000, "trillion"},
	{1000000000, "billion"},
	{1000000, "million"},
	{1000, "thousand"},
	{100, "hundred"},
}

type NumberLike int

func describeAt(n, pos int) string {

	for pos < len(powers) {

		pow := &powers[pos]
		pos++

		if n >= pow.scale {

			above, below := n/pow.scale, n%pow.scale
			s := describeAt(above, pos) + " " + pow.name
			if below != 0 {
				s += " " + describeAt(below, pos)
			}
			return s
		}
	}

	return belowHundred(n)
}

func describe(n int) string {
	return describeAt(n, 0)
}

func belowHundred(n int) string {
	if n < len(units) {
		return units[n]
	}

	ten, decimal := n/10, n%10

	r := tens[ten-2]
	if decimal > 0 {
		r += "-" + units[decimal]
	}

	return r
}

func Spell(number int) string {

	if number < 0 {
		return "minus " + describe(-number)
	}

	return describe(number)
}

func (number NumberLike) Spell() string {
	return Spell(int(number))
}
