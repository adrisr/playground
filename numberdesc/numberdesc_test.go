package numberdesc

import "testing"

type Testable testing.T

func (t *Testable) n(num int) {
	t.Logf("Number %d is '%s'", num, Spell(num))
}

func (t *Testable) Test() {
	t.n(13452345)
}

func TestSpeel(t *testing.T) {

	var tt *Testable = (*Testable)(t)
	tt.Test()
}
