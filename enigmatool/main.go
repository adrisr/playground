package main

import (
	"bitbucket.org/adrisr/playground/enigma"
	"fmt"
	"io"
	"log"
	"os"
	"strconv"
	"strings"
)

func main() {
	if len(os.Args) < 4 {
		log.Fatal("Not enough arguments")
	}
	switch os.Args[1] {
	case "generate", "gen":
		{
			nrotors, err := strconv.Atoi(os.Args[2])
			if err != nil {
				log.Fatal(err)
			}
			enigma, err := enigma.New(nrotors)
			if err != nil {
				log.Fatal(err)
			}
			err = enigma.Save(os.Args[3], false)
			if err != nil {
				log.Fatal(err)
			}
			log.Println("Saved enigma machine to", os.Args[3])
		}

	case "in", "input":
		k, err := enigma.Load(os.Args[2])
		if err == nil {
			err = k.SetKey(os.Args[3])
			for err == nil {
				var data [1]byte
				n, err2 := os.Stdin.Read(data[:])
				if err2 == nil {
					s := strings.ToUpper(string(data[:n]))
					for _, chr := range s {
						val, err := k.Input(byte(chr))
						if err == nil {
							fmt.Printf("%c", val)
						}
					}
				}
				err = err2
			}
			if err != nil && err != io.EOF {
				log.Fatal(err)
			}
		}
	}
}
