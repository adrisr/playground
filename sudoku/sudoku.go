package main

import (
	"errors"
	"fmt"
	"io"
	"math/rand"
	"strings"
)

const DEBUG_LEVEL = 1

type SudokuEntry struct {
	Value byte
	Mask  uint
}

type Sudoku struct {
	Content [9][9]*SudokuEntry
}

type KnownEntry struct {
	x, y  uint
	value byte
}

const PlaceHolders string = "?._-*="

func (this *Sudoku) Clone() *Sudoku {
	clone := &Sudoku{}
	for y := uint(0); y < 9; y++ {
		for x := uint(0); x < 9; x++ {
			cp := *this.Content[x][y]
			clone.Content[x][y] = &cp
		}
	}
	return clone
}

func (this *SudokuEntry) Solved() bool {
	return this.Value > 0
}

func (this *SudokuEntry) Masked(val uint) bool {
	return this.Value == byte(val) || (0 != this.Mask&(uint(1)<<val))
}

func NewSudokuEntry(value byte) *SudokuEntry {

	if value > 0 {
		return &SudokuEntry{Value: value}
	}

	entry := &SudokuEntry{Value: 0, Mask: 0x3fe}
	return entry
}

func NewSudoku(reader io.Reader) (*Sudoku, error) {
	s := &Sudoku{}

	for i := uint(0); i < 9; i++ {
		if err := s.readLine(reader, i); err != nil {
			return nil, err
		}
	}
	return s, nil
}

func (this *Sudoku) readLine(reader io.Reader, y uint) error {
	for x := 0; ; {
		var buf [1]byte
		_, err := reader.Read(buf[:])
		c := buf[0]

		switch {
		case err == io.EOF:
		case c == '\n':
			if x == 9 {
				return nil
			}
			return err

		case err != nil:
			return err

		case c == ' ':
		case c == '\t':
		case c == '\r':
			break

		default:
			if x >= 9 {
				return errors.New(fmt.Sprintf("Input line overrun at line %d pos %d (char [%x] %c)", y+1, x+1, c, c))
			}

			if c >= '1' && c <= '9' {
				this.Content[x][y] = NewSudokuEntry(c - '0')
			} else if strings.IndexByte(PlaceHolders, c) != -1 {
				this.Content[x][y] = NewSudokuEntry(0)
			} else {
				return errors.New(fmt.Sprintf("Invalid character at line %d pos %d (char [%x] %c)", y+1, x+1, c, c))
			}

			x++
		}
	}
}

func deMask(mask uint) []uint {
	val := make([]uint, 0)
	for i := uint(1); i < 10; i++ {
		if 0 != mask&(1<<i) {
			val = append(val, i)
		}
	}
	return val
}

func (this *Sudoku) Debug() {
	for y := 0; y < 9; y++ {
		for x := 0; x < 9; x++ {
			if item := this.Content[x][y]; item.Solved() {
				fmt.Printf("[%d,%d] : %d\n", x, y, item.Value)
			} else {
				fmt.Printf("[%d,%d] : %v\n", x, y, deMask(item.Mask))
			}

		}
	}
}

func (this *Sudoku) String() string {

	s := "/-----------\\\n"
	for y := 0; y < 9; y++ {
		var line [9]byte
		for x := 0; x < 9; x++ {
			if val := this.Content[x][y].Value; val > 0 {
				line[x] = '0' + val
			} else {
				line[x] = '.'
			}
		}
		s += fmt.Sprintf("|%s %s %s|\n", line[0:3], line[3:6], line[6:9])
		if y == 2 || y == 5 {
			s += "|           |\n"
		}
	}

	return s + "\\-----------/\n"
}

func (this *Sudoku) Solved() bool {
	for y := 0; y < 9; y++ {
		for x := 0; x < 9; x++ {
			if this.Content[x][y].Solved() == false {
				return false
			}
		}
	}

	return true
}

func (this *Sudoku) getKnownEntries() []KnownEntry {
	known := make([]KnownEntry, 0)

	for y := uint(0); y < 9; y++ {
		for x := uint(0); x < 9; x++ {
			if val := this.Content[x][y].Value; val > 0 {
				known = append(known, KnownEntry{x: x, y: y, value: val})
			}
		}
	}

	return known
}

func popCount(value uint) (uint, byte) {
	if value == 0 {
		return 0, 0
	}

	r := uint(0)
	last := byte(0)
	for i := uint(1); i < 10; i++ {
		if 0 != value&(1<<i) {
			r++
			last = byte(i)
		}
	}

	return r, last
}

func (this *Sudoku) SolveAtValue(px, py, val uint) bool {

	found := false

	for x := uint(0); !found && x < 9; x++ {
		if x != px {
			found = this.Content[x][py].Masked(val)
			if DEBUG_LEVEL > 1 && found {
				fmt.Printf("%dx%d can't be %d because of %dx%d (h)\n", px, py, val, x, py)
			}
		}
	}

	if !found {
		return true
	}

	found = false

	for y := uint(0); !found && y < 9; y++ {
		if y != py {
			found = this.Content[px][y].Masked(val)
			if DEBUG_LEVEL > 1 && found {
				fmt.Printf("%dx%d can't be %d because of %dx%d (v)\n", px, py, val, px, y)
			}
		}
	}

	if !found {
		return true
	}

	baseX := px - (px % 3)
	baseY := py - (py % 3)

	for _y := uint(0); _y < 3; _y++ {
		y := baseY + _y
		for _x := uint(0); _x < 3; _x++ {
			x := baseX + _x
			if x != px || y != py {
				if this.Content[x][y].Masked(val) {
					if DEBUG_LEVEL > 1 {
						fmt.Printf("%dx%d can't be %d because of %dx%d (s)\n", px, py, val, x, y)
					}
					return false
				}
			}
		}
	}

	return true
}

func (this *Sudoku) SolveAt(entry *SudokuEntry, x, y uint) (byte, error) {
	if entry.Mask == 0 {
		return 0, errors.New(fmt.Sprintf("At %dx%d : no possible values", x, y))
	}

	for c := uint(1); c <= 9; c++ {
		if entry.Masked(c) {
			if this.SolveAtValue(x, y, c) {
				return byte(c), nil
			}
		}
	}

	return 0, nil
}

func (this *Sudoku) getSolvedEntries() ([]KnownEntry, error) {
	known := make([]KnownEntry, 0)

	/*for y := uint(0); y < 9; y++ {
		for x := uint(0); x < 9; x++ {
			if entry := this.Content[x][y]; !entry.Solved() {
				if count, val := popCount(entry.Mask); count < 2 {
					if count == 0 {
						return known, errors.New(fmt.Sprintf("value at %dx%d is none", x, y))
					}

					entry.Value = val
					entry.Mask = 0
					known = append(known, KnownEntry{x: x, y: y, value: val})
				}
			}
		}
	}*/
	for y := uint(0); y < 9; y++ {
		for x := uint(0); x < 9; x++ {
			if entry := this.Content[x][y]; !entry.Solved() {
				if val, err := this.SolveAt(entry, x, y); err == nil {
					if val > 0 {
						entry.Value = val
						entry.Mask = 0
						known = append(known, KnownEntry{x: x, y: y, value: val})
					}
				} else {
					return known, err
				}
			}
		}
	}

	return known, nil
}

func (this *SudokuEntry) resolve(x, y uint, k KnownEntry) error {

	if this.Solved() {
		if k.value != this.Value {
			return nil
		}

		return errors.New(fmt.Sprintf("At %d,%d : collision value %d", x, y, k.value))
	}

	pattern := uint(1) << k.value
	if 0 != (this.Mask & pattern) {
		this.Mask &^= pattern
	}

	return nil
}

func (this *Sudoku) resolve(k KnownEntry) error {
	// none at this line can have same value

	for x := uint(0); x < 9; x++ {
		if x != k.x {
			err := this.Content[x][k.y].resolve(x, k.y, k)
			if err != nil {
				fmt.Println("Failed horizontal")
				return err
			}
		}
	}
	for y := uint(0); y < 9; y++ {
		if y != k.y {
			err := this.Content[k.x][y].resolve(k.x, y, k)
			if err != nil {
				fmt.Println("Failed vertical")
				return err
			}
		}
	}
	baseX := 3 * (k.x / 3)
	baseY := 3 * (k.y / 3)

	for _y := uint(0); _y < 3; _y++ {
		y := baseY + _y
		for _x := uint(0); _x < 3; _x++ {
			x := baseX + _x
			if x != k.x || y != k.y {
				err := this.Content[x][y].resolve(x, y, k)
				if err != nil {
					fmt.Printf("Failed group %dx%d vs %dx%d", x, y, k.x, k.y)
					return err
				}
			}
		}
	}

	return nil
}

func (this *Sudoku) Solve() error {
	return this.SolveEx(true)
}

func (this *Sudoku) SetRandom() (uint, uint, byte) {
	for {
		x, y := rand.Intn(9), rand.Intn(9)
		if !this.Content[x][y].Solved() {
			vals := deMask(this.Content[x][y].Mask)
			val := byte(vals[rand.Intn(len(vals))])

			this.Content[x][y].Value = val
			this.Content[x][y].Mask = 0

			return uint(x), uint(y), val
		}
	}
}

func (this *Sudoku) SolveEx(guessing bool) error {

	iter := 0
	for k := this.getKnownEntries(); len(k) > 0; {

		for _, known := range k {
			err := this.resolve(known)
			if err != nil {
				return err
			}
		}

		var err error
		k, err = this.getSolvedEntries()
		if err != nil {
			return err
		}

		iter++
		fmt.Printf("At iter %d solved %d:\n%s\n", iter, len(k), this.String())
	}

	if !this.Solved() && guessing {
		for {
			clone := this.Clone()
			x, y, val := clone.SetRandom()
			fmt.Printf("Unable to solve analytically. Guessing ... %dx%d = %d\n",
				x, y, val)
			err := clone.SolveEx(false)
			if err == nil {
				this = clone.Clone()
				break
			}
		}
	}

	if this.Solved() {
		fmt.Printf("Solved after %d iters:\n%s\n", iter, this.String())
	} else {
		fmt.Printf("Unable to solve after %d iters:\n%s\n", iter, this.String())
		this.Debug()
		return errors.New("Unable to solve")
	}

	return nil
}

func main() {
	_ = " " +
		"... ..3 ...\n" +
		".5. ... 31.\n" +
		"3.. 29. .4.\n" +
		"2.. 3.. ..5\n" +
		"1.8 ... 4..\n" +
		".93 ..1 7..\n" +
		"... 75. ...\n" +
		"... 6.. .2.\n" +
		"... .38 9..\n"

	_ = " " + // impossible
		"8.. ... ...\n" +
		"..3 6.. ...\n" +
		".7. .9. 2..\n" +
		".5. ..7 ...\n" +
		"... .45 7..\n" +
		"... 1.. .3.\n" +
		"..1 ... .68\n" +
		"..8 5.. .1.\n" +
		".9. ... 4..\n"

	_ = " " + // evil
		".58 ..3 .29\n" +
		"..6 .7. ..8\n" +
		"... ... 63.\n" +
		"..5 ... 49.\n" +
		".3. ..5 .1.\n" +
		"6.. ... ...\n" +
		"..2 3.. ...\n" +
		"... 8.1 ..3\n" +
		"9.. 5.2 ...\n"

	sudoku := " " +
		"96. 2.. 18.\n" +
		"17. .4. ..6\n" +
		"82. 1.6 ...\n" +
		"397 651 ..8\n" +
		"641 ..2 395\n" +
		"258 ..4 671\n" +
		".39 41. 862\n" +
		".86 32. .14\n" +
		"412 .6. 53.\n"

	s, err := NewSudoku(strings.NewReader(sudoku))
	if err != nil {
		panic(err)
	}

	fmt.Print(s)
	err = s.Solve()
	if err != nil {
		panic(err)
	}
}
