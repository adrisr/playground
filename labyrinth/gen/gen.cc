#include <iostream>
#include <array>
#include <vector>
#include <queue>
#include <inttypes.h>
#include <cassert>
#include <cstdlib>

struct Pos {
    enum Direction {
        LEFT = 0,
        UP,
        RIGHT,
        DOWN
    };

    uint x, y;

    Pos(uint x, uint y)  : x(x), y(y) {}
    Pos() : Pos(0, 0) {}

    Pos left() const { return Pos(x-1,y); }
    Pos right() const { return Pos(x+1,y); }
    Pos up() const { return Pos(x,y-1); }
    Pos down() const { return Pos(x,y+1); }
    Pos coord(Direction dir) {
        switch(dir) {
            case LEFT:  return left();
            case UP:    return up();
            case RIGHT: return right();
            case DOWN:  return down();
            default:
                assert(false);
        }
    }
};

class Map {
public:

    Map(uint width,
        uint height,
        Pos start,
        Pos end)
        : w_(width)
        , h_(height)
        , start_(start)
        , end_(end) {
            build();
    }
    
    size_t index(Pos pos) const {
        return pos.y*w_ + pos.x;
    }
    
    bool get(Pos pos) const {
        assert(in_range(pos));
        return map_[index(pos)];
    }

    bool clear(Pos pos) {
        bool clear_prev = clear_unchecked(pos);
        assert(clear_prev);
        return clear_prev;
    }

    bool set(Pos pos) {
        bool set_prev = set_unchecked(pos);
        assert(!set_prev);
        return set_prev;
    }

    bool set_unchecked(Pos pos) {
        assert(in_range(pos));
        bool val = true;
        std::swap(val, map_[index(pos)]);
        return val;
    }

    bool clear_unchecked(Pos pos) {
        assert(in_range(pos));
        bool val = false;
        std::swap(val, map_[index(pos)]);
        return val;
    }

    void print(std::ostream& out) {
        Pos p(0, 0);
        for (;p.y<h_;++p.y) {
            for (p.x=0;p.x<w_;++p.x) {
                out << (get(p)? '#' : ' ');
            }
            out << '\n';
        }
    }

private:
    
    void build() {
        map_.resize(w_ * h_);
        size_t i;
        for (i=0;i<w_;++i) {
            set(Pos(i, 0));
            set(Pos(i, h_-1));
        }
        for (i=1;i<h_-1;++i) {
            set(Pos(0, i));
            set(Pos(w_-1, i));
        }
        clear_unchecked(start_);
        clear_unchecked(end_);
        
        for (size_t y=1;y<h_-1;++y) {
            for (size_t x=2;x<w_-2;x+=2) {
                if ( rand() % 2 )
                /*if ( (x + y) % 3 )*/ set(Pos(x, y));
            }
        }

        for (i=4;i>1;--i) {
            std::vector<Pos> v;
            Pos pos(1, 1);
            for (;pos.y<h_-1;++pos.y)
                for (pos.x=1;pos.x<w_-1;++pos.x)
                    if (connectivity(pos)==i)
                        v.push_back(pos);
            std::cerr << "conn[" << i << "]: " << v.size() << '\n';
            while (v.size()) {
                size_t r = rand() % v.size();
                Pos pos = v[r];
                std::swap(v[r], v.back());
                v.resize(v.size()-1);
                if (connectivity(pos) != i) continue;

                bool skip = false;
                for (int dir=0;dir<4 && !skip;++dir) {
                    Pos p = pos.coord((Pos::Direction)dir);
                    //skip = in_range(p) && connectivity(p)<2;
                }
                if (skip) continue;

                set(pos);
                if (!valid()) clear(pos);
            }
        }
    }
    
    int connectivity(Pos pos) const {
        int value = 0;
        if (get(pos)) return value;
        for (int i=0;i<4;++i) {
            Pos p = pos.coord((Pos::Direction)i);
            if (in_range(p) && !get(p)) ++value;
        }
        return value;
    }

    bool in_range(Pos p) const {
        return p.x < w_ && p.y < h_;
    }
    
    bool valid() const {
        auto copy = *this;
        copy.fill(start_);
        //copy.print(std::cout);
        return !copy.has_empty();
    }
    
    void fill(Pos p) {
        std::queue<Pos> q;
        q.push(p);
        while (!q.empty()) {
            auto pos = q.front();
            q.pop();
            if (!get(pos)) {
                set(pos);
                for (int dir=0; dir<4; ++dir) {
                    p = pos.coord((Pos::Direction)dir);
                    if (in_range(p) && !get(p)) {
                        q.push(p);
                    }
                }
            }
        }
    }

    bool has_empty() const {
        return std::find(map_.cbegin(),
                         map_.cend(),
                         false) != map_.cend();
    }

    uint w_,
         h_;
    Pos start_,
        end_;
    std::vector<bool> map_;
};

int main(int argc, char *argv[]) {
    
    uint width = std::atoi(argv[1]);
    uint height = std::atoi(argv[2]);

    Pos start{0, height/3},
        end{width-1, height*2/3};
    
    sranddev();
    Map map(width, height, start, end);
    map.print(std::cout);
    return 0;
}

