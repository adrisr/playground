#include "config.h"
#include "log.h"
#include "request.h"

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <signal.h>
#include <errno.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/wait.h>
#include <unistd.h>

static void print_connection(const struct sockaddr *addr) {
    void *address = NULL;
    uint16_t port;
    switch (addr->sa_family) {
        case AF_INET:
            address = &((struct sockaddr_in*)addr)->sin_addr;
            port = ((struct sockaddr_in*)addr)->sin_port;
            break;
        case AF_INET6:
            address = &((struct sockaddr_in6*)addr)->sin6_addr;
            port = ((struct sockaddr_in6*)addr)->sin6_port;
    }

    char buf[INET6_ADDRSTRLEN];
    if (NULL != address
     && NULL != inet_ntop(addr->sa_family, address, buf, sizeof(buf))) {
        buf[sizeof(buf)-1] = '\0';
        LOG("New client %s:%u", buf, ntohs(port));
    } else {
        LOG("Connection from unknown address");
    }
}

static int main_loop(uint16_t listen_port) {

    int server = socket(PF_INET, SOCK_STREAM, 0);
    if (server == -1) {
        perror("Unable to create socket");
        return 1;
    }

    const int reuse_flag = 1;
    if (setsockopt(server,
                   SOL_SOCKET,
                   SO_REUSEADDR,
                   &reuse_flag,
                   sizeof(int)) == -1) {
            perror("setsockopt failed");
            close_fd(server);
            return 1;
        }

    struct sockaddr_in bind_address = {0};
    bind_address.sin_family = AF_INET;
    bind_address.sin_port = htons(listen_port);
    bind_address.sin_addr.s_addr = htonl(INADDR_ANY);

    if (bind(server,
            (struct sockaddr*) &bind_address,
            sizeof(bind_address)) == -1) {
        perror("Bind failed");
        close_fd(server);
        return 1;
    }

    if (listen(server, BACKLOG) == -1) {
        perror("Unable to set socket to listening mode");
        close_fd(server);
        return 1;
    }

    LOG("Listening for connections in port %d", listen_port);

    /* run until killed */
    for (;;) {
        union {
            struct sockaddr_in addr4;
            struct sockaddr_in6 addr6;
        } addr;
        socklen_t addr_len = sizeof(addr);
        int client = accept(server,
                          (struct sockaddr*) &addr.addr4,
                          (socklen_t*) &addr_len);

        if (client == -1) {
            if (errno == EINTR) continue;
            perror("accept() failed");
            close_fd(server);
            return 1;
        }

        print_connection((struct sockaddr *)&addr.addr4);
        pid_t pid = fork();
        switch (pid) {
            case -1:
                perror("Unable to fork");
                close_fd(client);
                close_fd(server);
                return 1;

            case 0:
                log_set_name("client");
                close_fd(server);
                handle_request(client);
                close_fd(client);
                exit(0);
                break;

            default:
                close_fd(client);
                break;
        }
    }

    return 0;
}

static void sigchld_handler(int signal) {
    if (signal == SIGCHLD) {
        int saved_errno = errno;
        /* loop so more than one child can be reaped. Not blocking */
        while (waitpid((pid_t)(-1), 0, WNOHANG) > 0)
            ;
        errno = saved_errno;
    }
}


int main(int argc, char *argv[]) {
    log_set_name("server");

    /* install SIGCHLD handler to reap zombie processes */
    struct sigaction act;
    act.sa_handler = (void*)&sigchld_handler;
    sigemptyset(&act.sa_mask);
    act.sa_flags = SA_NOCLDSTOP;

    if (sigaction(SIGCHLD, &act, 0) == -1) {
      perror("sigaction() failed");
      return 1;
    }

    return main_loop(LISTEN_PORT);
}

