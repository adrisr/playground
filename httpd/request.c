#include "config.h"
#include "log.h"
#include "request.h"

#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <errno.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/socket.h>

#define HTTP_VERSION_LENGTH 8 /* length of "HTTP/1.x" */

#define RESULT_OK 200
#define RESULT_BAD_REQUEST 400
#define RESULT_FORBIDDEN 403
#define RESULT_NOT_FOUND 404
#define RESULT_TOO_LARGE 413
#define RESULT_SERVER_ERROR 500
#define RESULT_NOT_IMPLEMENTED 501
#define RESULT_VERSION_NOT_SUPPORTED 505

static char request_is_complete(char *buffer, size_t length) {
    char prev_was_lf = 0,
         found = 0;
    for (size_t i=0; !found && i<length; ++i) {
        char c = buffer[i];
        if (c != '\n') {
            if (c != '\r') prev_was_lf = 0;
        } else {
            found = prev_was_lf;
            prev_was_lf = 1;
        }
    }
    return found;
}

static size_t read_data(int fd, char *buffer, size_t max_length) {
    size_t received = 0;
    ssize_t nread;
    do {
        nread = recv(fd, buffer + received, max_length - received, 0);
        if (nread > 0) {
            received += nread;
        }
    }
    while (received < max_length
        && !request_is_complete(buffer, received)
        && (nread > 0 || (nread == -1 && errno == EINTR)));

    if (nread == -1) {
        LOG("Read failed: [%d] %s", errno, strerror(errno));
        return 0;
    }

    return received;
}

static char send_data(int sock, char *buffer, size_t length) {

    size_t sent = 0;
    ssize_t nwrite;
    do {
        nwrite = send(sock, &buffer[sent], length - sent, MSG_NOSIGNAL);
        if (nwrite>0) sent += nwrite;
    }
    while (sent < length && (nwrite>=0 || errno==EINTR));
    if (nwrite == -1) {
        LOG("Write failed: [%d] %s", errno, strerror(errno));
    }
    return sent == length;
}

int parse_request(char *request, size_t len, char **path_ptr, char *version_ptr) {

    size_t pos = 0;
    *version_ptr = '0';

#   define SKIP_UNTIL(CONDITION) do {               \
        while(pos<len && !(CONDITION)) ++pos; \
    } while(0)

    SKIP_UNTIL(request[pos]==' ');
    size_t method_end = pos;

    SKIP_UNTIL(request[pos]!=' ');
    size_t path_start = pos;

    SKIP_UNTIL(request[pos]==' ');
    size_t path_end = pos;

    SKIP_UNTIL(request[pos]!=' ');
    size_t version_start = pos;

    SKIP_UNTIL(request[pos]=='\r' || request[pos]=='\n');
    size_t version_end = pos;

    if (method_end == 0
     || method_end > MAX_METHOD_LENGTH
     || path_end - path_start > MAX_PATH_LENGTH
     || request[path_start] != '/'
     || version_end - version_start != HTTP_VERSION_LENGTH)
        return RESULT_BAD_REQUEST;

    if (method_end != 3 || strncasecmp(request, "GET", method_end))
        return RESULT_NOT_IMPLEMENTED;

    char minor_version = request[version_end-1];
    if (strncasecmp(&request[version_start], "HTTP/1.", HTTP_VERSION_LENGTH-1)
     || (minor_version!='0' && minor_version!='1'))
        return RESULT_VERSION_NOT_SUPPORTED;

    request[path_end] = '\0';
    *path_ptr = &request[path_start];
    *version_ptr = minor_version;
    return 0;
}

static const char *http_code_string(int code) {
    switch (code) {
        case RESULT_OK: return "OK";
        case RESULT_BAD_REQUEST: return "Bad Request";
        case RESULT_FORBIDDEN: return "Forbidden";
        case RESULT_NOT_FOUND: return "Not Found";
        case RESULT_TOO_LARGE: return "Too Large";
        case RESULT_SERVER_ERROR: return "Server Error";
        case RESULT_NOT_IMPLEMENTED: return "Not Implemented";
        case RESULT_VERSION_NOT_SUPPORTED: return "Version not supported";
        default: return "Unexpected";
    }
}

static char send_response(int sock,
                         int code,
                         char minor_version,
                         char *content_type,
                         ssize_t body_length) {

#   define BUF_SIZE 128
    char length_header[BUF_SIZE],
         ctype_header[BUF_SIZE],
         response[MAX_RESPONSE_HEADERS_SIZE];

    if (body_length >= 0) {
        snprintf(length_header, BUF_SIZE, "Content-Length: %ld", body_length);
    } else {
        snprintf(length_header, BUF_SIZE, "Transfer-Encoding: chunked");
    }

    if (content_type) {
        snprintf(ctype_header, BUF_SIZE, "Content-Type: %s\r\n", content_type);
    } else {
        ctype_header[0] = '\0';
    }

    int length = snprintf(response, MAX_RESPONSE_HEADERS_SIZE,
                    "HTTP/1.%c %d %s\r\n"
                    "Server: httpd-by-asr/1.0\r\n"
                    "Connection: close\r\n"
                    "%s\r\n"
                    "%s"
                    "\r\n",
                    minor_version, code, http_code_string(code),
                    length_header,
                    ctype_header);

    // TODO: use dynamic memory
    if (length > MAX_RESPONSE_HEADERS_SIZE) {
        LOG("Response headers too large. Truncated");
        length = MAX_RESPONSE_HEADERS_SIZE;
    }

    return send_data(sock, response, length);
#   undef BUF_SIZE
}

static void error_response(int sock, int code, char minor_version) {
    LOG("Error %d", code);
    send_response(sock, code, minor_version, NULL, 0);
}

static char *resolve_path(char *path) {
    size_t path_len = strlen(path),
           base_len = strlen(BASE_DIR),
           len = path_len + base_len + 1;
    char *result = (char*) malloc(len);
    if (result) {
        int skip_slash = path_len>0 && path[0] == '/';
        memcpy(result, BASE_DIR, base_len);
        memcpy(result + base_len, path + skip_slash, path_len + 1 - skip_slash);
        char *ptr;
        /* TODO: parse ".." manually to prevent escaping base directory
         * right now just removing .. to prevent it, not correct */
        while ( (ptr=strstr(result, "..")) != NULL) {
            memmove(ptr, ptr + 2, strlen(ptr) - 2 + 1); /* move also '\0' */
        }

        /* Remove any leading / */
        for (ptr=result; ptr[0] == '/'; ++ptr)
            ;
        if (ptr != result)
            memmove(result, ptr, strlen(ptr) + 1);
    }
    return result;
}

static void send_content(int sock, int file, size_t length) {
#define BUF_SIZE 4096
    char buf[BUF_SIZE],
         sent;
    ssize_t nread;
    size_t total = 0;
    do {
        nread = read(file, buf, BUF_SIZE);
        if (nread>0) {
            sent = send_data(sock, buf, nread);
            total += nread;
        } else if (nread == -1 && errno == EINTR) {
            sent = 1;
        }
        else {
            sent = 0;
        }
    } while(sent && total < length);
#undef BUF_SIZE
}

static void send_file(int sock, char *path, char minor_version) {
    int fd = open(path, O_RDONLY);
    if (fd != -1) {
        off_t length = lseek(fd, 0L, SEEK_END);
        if (length != (off_t)(-1)) {
            lseek(fd, 0L, SEEK_SET);
            LOG("sending file %s (%ld bytes)", path, length);
            if (send_response(sock, RESULT_OK, minor_version,
                              DEFAULT_CONTENT_TYPE, length)) {
                send_content(sock, fd, length);
            }
        } else {
            error_response(sock, RESULT_TOO_LARGE, minor_version);
        }
        close_fd(fd);
    } else {
        error_response(sock, RESULT_FORBIDDEN, minor_version);
    }
}

static void handle_get(int sock, char *http_path, char minor_version) {
    struct stat st;
    char *path = resolve_path(http_path);
    if (path != NULL) {
        if (0==stat(path, &st)) {
            if (S_ISDIR(st.st_mode)) {
                // TODO: list directory contents
                LOG("GET %s [%d]", path, RESULT_FORBIDDEN);
                error_response(sock, RESULT_FORBIDDEN, minor_version);
            } else {
                send_file(sock, path, minor_version);
            }
        } else {
            LOG("GET %s [%d]", path, RESULT_NOT_FOUND);
            error_response(sock, RESULT_NOT_FOUND, minor_version);
        }
        free(path);
    } else {
        LOG("GET %s [%d]", path, RESULT_SERVER_ERROR);
        error_response(sock, RESULT_SERVER_ERROR, minor_version);
    }
}

void handle_request(int sock) {
    char request[MAX_REQUEST_SIZE];

    LOG("Handling request");
    size_t length = read_data(sock, request, MAX_REQUEST_SIZE);

    char *path;
    char minor_version;
    int error = parse_request(request, length, &path, &minor_version);
    if (error == 0) {
        handle_get(sock, path, minor_version);
    } else {
        error_response(sock, error, minor_version);
    }

}

int close_fd(int fd) {
    int result;
    /* safe close a descriptor even if there is an interruption */
    while ( (result=close(fd)) == -1 && errno == EINTR)
        ;
    return result;
}

