package enigma

import (
	"bytes"
	"crypto/rand"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"strings"
)

// debug settings
const LOG_ENABLED bool = false

const N byte = 26

// 					  01234567890123456789012345
const codes string = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"

type RotorSide [N]byte
type Rotor struct {
	Settings        RotorSide
	inverseSettings RotorSide
	offset          byte
	counter         byte
}

type Enigma struct {
	Plugboard Rotor
	Rotor     []*Rotor
	Reverser  RotorSide
}

func charToCode(c byte) (byte, error) {
	pos := strings.IndexByte(codes, c)
	if pos < 0 {
		return 0, errors.New(fmt.Sprintf("Non-translatable character: %q", c))
	}
	if pos >= int(N) {
		return 0, errors.New(fmt.Sprintf("charToCode assertion failed"))
	}
	return byte(pos), nil
}

func codeToChar(c byte) (byte, error) {
	if c >= N {
		return 0, errors.New(fmt.Sprintf("codeToChar assertion failed"))
	}
	return codes[c], nil
}

func normalize(val int) byte {
	switch {
	case val < 0:
		for val < 0 {
			val += int(N)
		}
	case val >= int(N):
		for val >= int(N) {
			val -= int(N)
		}
	}
	return byte(val)
}

func (side *RotorSide) Apply(value byte, rotor *Rotor) byte {
	var offset int
	if rotor != nil {
		offset = int(rotor.offset) + int(rotor.counter)
	}
	idx := normalize(int(value) - offset)
	val := side[idx]
	r := normalize(int(val) + offset)
	if LOG_ENABLED {
		log.Printf("Apply value:%d offset:%d idx:%d val:%d r:%d",
			value, offset, idx, val, r)
	}
	return r
}

func (machine *Enigma) Increment(idx int) {
	if idx < len(machine.Rotor) {
		machine.Rotor[idx].counter++
		if machine.Rotor[idx].counter >= N {
			machine.Rotor[idx].counter = 0
			machine.Increment(idx + 1)
		}
		if LOG_ENABLED {
			log.Printf("Rotor[%d] pos %d", idx, machine.Rotor[idx].counter)
		}
	}
}
func (machine *Enigma) Input(c byte) (byte, error) {
	v, err := charToCode(c)
	if err != nil {
		return 0, err
	}
	v = machine.Plugboard.Settings.Apply(v, nil)

	nr := len(machine.Rotor)
	if LOG_ENABLED {
		log.Printf("Encode -> %d [%d]", v, nr)
	}

	for i := 0; i < nr; i++ {
		rotor := machine.Rotor[i]
		v = rotor.Settings.Apply(v, rotor)
		if LOG_ENABLED {
			log.Printf("Rotor[%d] -> %d", i, v)
		}
	}
	v = machine.Reverser.Apply(v, nil)
	for i := 0; i < nr; i++ {
		pos := nr - i - 1
		rotor := machine.Rotor[pos]
		v = rotor.inverseSettings.Apply(v, rotor)
		if LOG_ENABLED {
			log.Printf("Rotor[%d] <- %d", pos, v)
		}
	}
	v = machine.Plugboard.inverseSettings.Apply(v, nil)
	machine.Increment(0)
	return codeToChar(v)
}

func (machine *Enigma) Save(path string, indent bool) error {
	b, err := json.Marshal(machine)
	if err == nil {
		var f *os.File

		f, err = os.Create(path)
		if err == nil {
			if indent {
				var buf bytes.Buffer
				json.Indent(&buf, b, "", "\t")
				_, err = buf.WriteTo(f)
			} else {
				_, err = f.Write(b)
			}
		}
	}
	return err
}

func randN() (byte, error) {
	return randVal(N)
}

func randVal(val byte) (byte, error) {
	var b [1]byte
	_, err := rand.Read(b[:])
	if err != nil {
		return 0, err
	}
	return b[0] % val, nil
}

func randNotVal(val byte) (byte, error) {
	v, err := randVal(N - 1)
	k := (val + 1 + v) % N
	if k == val {
		return k, errors.New("randNotVal assertion failed")
	}
	return k, err
}

func (rotor *Rotor) fillInverseSettings() {
	for i := byte(0); i < N; i++ {
		rotor.inverseSettings[rotor.Settings[i]] = i
	}

}

func (device *Enigma) prepare() {
	device.Plugboard.fillInverseSettings()
	for _, rotor := range device.Rotor {
		rotor.fillInverseSettings()
	}
}

func randomRotor(rotor *Rotor) error {
	for i := byte(0); i < N; i++ {
		rotor.Settings[i] = i
	}

	for i := byte(0); i < N; i++ {
		tmp := rotor.Settings[i]
		pos, err := randN()
		if err != nil {
			return err
		}
		if pos != i {
			rotor.Settings[i] = rotor.Settings[pos]
			rotor.Settings[pos] = tmp
		}
	}
	return nil
}

func randomReverser(rotor *RotorSide) error {

	var used [N]bool

	for i := byte(0); i < N; i++ {
		for !used[i] {
			pos, err := randNotVal(i)
			if err != nil {
				return err
			}
			if !used[pos] && i != pos {
				rotor[i] = pos
				rotor[pos] = i
				used[i] = true
				used[pos] = true
			}
		}
	}

	return nil
}

func New(numRotots int) (*Enigma, error) {
	machine := &Enigma{}
	err := randomRotor(&machine.Plugboard)
	if err != nil {
		return nil, err
	}
	machine.Rotor = make([]*Rotor, numRotots)
	for i := 0; i < numRotots; i++ {
		machine.Rotor[i] = &Rotor{}
		err := randomRotor(machine.Rotor[i])
		if err != nil {
			return nil, err
		}
	}
	err = randomReverser(&machine.Reverser)
	if err != nil {
		return nil, err
	}

	machine.prepare()

	return machine, nil
}

func Load(path string) (*Enigma, error) {
	var k Enigma
	f, err := os.Open(path)
	if err == nil {
		data, err := ioutil.ReadAll(f)
		if err == nil {
			err = json.Unmarshal(data, &k)
		}
	}
	k.prepare()
	return &k, err
}

func (machine *Enigma) SetKey(key string) error {
	nr := len(machine.Rotor)
	if len(key) != nr {
		msg := fmt.Sprintf("Key length mismatch. Machine has %d rotors, passed key of length %d", nr, len(key))
		return errors.New(msg)
	}
	for i, k := range key {
		pos := nr - i - 1 // because rotors are laid right to left
		val, err := charToCode(byte(k))
		if err != nil {
			return err
		}
		machine.Rotor[pos].counter = 0
		machine.Rotor[pos].offset = val
	}
	return nil
}
